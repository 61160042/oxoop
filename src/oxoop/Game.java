/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxoop;

import java.util.Scanner;

/**
 *
 * @author dell
 */
public class Game {

    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;

    Game() {
        o = new Player('O');
        x = new Player('X');
    }

    public void run() {
        this.startGame();
        this.showWelcome();
        do {
            this.showTable();
            this.showTurn();
            this.runOnce();
            this.showScore(o);
            this.showScore(x);
            this.startGame();
        } while (this.inputContinue());
        this.showGoodBye();
    }

    public void runOnce() {
        while (true) {
            this.inputRowCol();
            this.showTable();
            if (table.checkWin()) {
                this.showWin();
                break;
            }
            if (table.checkDraw()) {
                this.showDraw();
                break;
            }
            table.switchTurn();
            this.showTurn();
        }
    }

    public void startGame() {
        table = new Table(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game!");
    }

    public void showTable() {
        for (int i = 0; i < table.getData().length; i++) {
            for (int j = 0; j < table.getData()[i].length; j++) {
                System.out.print(table.getData()[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn :");
    }

    public void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input Row Col >> ");
        try {
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            table.setRowCol(row, col);
        } catch (Exception e) {
            this.showWrongPosition();
            kb.nextLine();
            this.inputRowCol();
        }
    }

    public boolean inputContinue() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Continue?(y/n) : ");
        char answer = kb.next().charAt(0);
        while(answer!='y' && answer!='n'){
            System.out.print("Continue?(y/n) : ");
            answer = kb.next().charAt(0);
        }
        if (answer == 'y') {
            return true;
        }
        return false;
    }

    public void showGoodBye() {
        System.out.println("Good Bye...");
    }

    public void showScore(Player player) {
        System.out.println("Player "+player.getName()+" : win = " + 
                player.getWin() + " , lose = " + player.getLose()
                + " , draw = " + player.getDraw());
    }

    public void showWin() {
        System.out.println("----" + table.getWinner().getName() + " Win!!!----");
    }

    public void showDraw() {
        System.out.println("----Draw!!!----");
    }

    public void showWrongPosition() {
        System.out.println("This position is wrong!!!");
    }

}

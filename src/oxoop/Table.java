/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxoop;
import java.util.Random;
/**
 *
 * @author dell
 */
public class Table {
    private char[][] data = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;
    
    Table(Player o,Player x){
        this.o=o;
        this.x=x;
        Random rand = new Random();
        currentPlayer=this.random(rand.nextInt(2));
    }
    
    public Player random(int countPlayer){
        if(countPlayer==0){
            return o;
        }
        return x;
    }
    public char[][] getData(){
        return data;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void setRowCol(int row,int col) throws Exception{
        if(this.getData()[row][col]=='-'){
            data[row][col]=currentPlayer.getName();
        }
        else{
            throw new Exception();
        }
    }
    
    public boolean checkWin() {
        if (checkWinRow() || checkWinCol() || checkWinDiagonal()) {
            win=currentPlayer;
            if(currentPlayer==o){
                    o.win();
                    x.lose();
                }
            else{
                x.win();
                o.lose();
            }
            return true;
        }
        return false;
    }

    public boolean checkWinRow() {
        for (int row = 0; row < data.length; row++) {
            if (data[row][0] == data[row][1] && data[row][0] == data[row][2]
                    && data[row][0] != '-') {
                return true;
            }
        }
        return false;
    }

    public boolean checkWinCol() {
        for (int col = 0; col < data.length; col++) {
            if (data[0][col] == data[1][col] && data[0][col] == data[2][col]
                    && data[0][col] != '-') {
                return true;
            }
        }
        return false;
    }

    public boolean checkWinDiagonal() {
        if (data[0][0] == data[1][1] && data[1][1] == data[2][2]
                && data[0][0] != '-') {
            return true;
        } else if (data[0][2] == data[1][1] && data[1][1] == data[2][0]
                && data[0][2] != '-') {
            return true;
        }
        return false;
    }
    
    public void switchTurn(){
        if(currentPlayer==o){
            currentPlayer=x;
        }
        else{
            currentPlayer=o;
        }
    }
    
    public Player getWinner(){
        return win;
    }
    
    public boolean checkDraw() {
        int sum = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (data[i][j] != '-') {
                    sum++;
                }
            }
        }
        if (sum == 9 && !checkWin()) {
            o.draw();
            x.draw();
            return true;
        }
        return false;
    }
}
